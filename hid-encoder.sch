EESchema Schematic File Version 2
LIBS:hid-encoder
LIBS:atmel
LIBS:device
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "HIDe"
Date "Saturday, August 01, 2015"
Rev "1.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AT90USB162 U2
U 1 1 5192B8B7
P 8500 3950
F 0 "U2" H 9250 3050 60  0000 C CNN
F 1 "AT90USB162" H 8700 3700 60  0000 C CNN
F 2 "" H 8500 3950 60  0001 C CNN
F 3 "" H 8500 3950 60  0001 C CNN
	1    8500 3950
	1    0    0    -1  
$EndComp
$Comp
L ENCODER_QUADRATURE_PB E1
U 1 1 5192B94C
P 6600 4650
F 0 "E1" H 6600 5000 60  0000 C CNN
F 1 "ENCODER_QUADRATURE_PB" H 6600 4350 60  0000 C CNN
F 2 "~" H 6250 4600 60  0000 C CNN
F 3 "~" H 6250 4600 60  0000 C CNN
	1    6600 4650
	-1   0    0    1   
$EndComp
$Comp
L C C2
U 1 1 5192B95B
P 9150 3000
F 0 "C2" H 9200 3100 50  0000 L CNN
F 1 "1uF" H 9200 2900 50  0000 L CNN
F 2 "" H 9150 3000 60  0001 C CNN
F 3 "" H 9150 3000 60  0001 C CNN
	1    9150 3000
	0    -1   -1   0   
$EndComp
$Comp
L C C3
U 1 1 524A0C51
P 6750 3250
F 0 "C3" H 6800 3350 50  0000 L CNN
F 1 "12 pF" H 6800 3150 50  0000 L CNN
F 2 "" H 6750 3250 60  0001 C CNN
F 3 "" H 6750 3250 60  0001 C CNN
	1    6750 3250
	0    -1   -1   0   
$EndComp
$Comp
L C C5
U 1 1 524A0C60
P 6750 3950
F 0 "C5" H 6800 4050 50  0000 L CNN
F 1 "12 pF" H 6800 3850 50  0000 L CNN
F 2 "" H 6750 3950 60  0001 C CNN
F 3 "" H 6750 3950 60  0001 C CNN
	1    6750 3950
	0    -1   -1   0   
$EndComp
Text Label 6300 3600 0    60   ~ 0
GND
$Comp
L R R2
U 1 1 524A0F33
P 8700 2150
F 0 "R2" V 8780 2150 50  0000 C CNN
F 1 "22" V 8700 2150 50  0000 C CNN
F 2 "" H 8700 2150 60  0001 C CNN
F 3 "" H 8700 2150 60  0001 C CNN
	1    8700 2150
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 524A0F42
P 8500 2150
F 0 "R1" V 8580 2150 50  0000 C CNN
F 1 "22" V 8500 2150 50  0000 C CNN
F 2 "" H 8500 2150 60  0001 C CNN
F 3 "" H 8500 2150 60  0001 C CNN
	1    8500 2150
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 524A1049
P 8050 2500
F 0 "C1" H 8100 2600 50  0000 L CNN
F 1 "10uF" H 8100 2400 50  0000 L CNN
F 2 "" H 8050 2500 60  0001 C CNN
F 3 "" H 8050 2500 60  0001 C CNN
	1    8050 2500
	0    -1   -1   0   
$EndComp
Text Label 7650 2500 0    60   ~ 0
GND
Text Label 9550 3000 2    60   ~ 0
GND
Text Label 8100 2150 0    60   ~ 0
VCC
Text Label 7550 4150 0    60   ~ 0
VCC
$Comp
L R R3
U 1 1 524A12AE
P 10050 3850
F 0 "R3" V 10130 3850 50  0000 C CNN
F 1 "10k" V 10050 3850 50  0000 C CNN
F 2 "" H 10050 3850 60  0001 C CNN
F 3 "" H 10050 3850 60  0001 C CNN
	1    10050 3850
	0    -1   -1   0   
$EndComp
Text Label 10550 3850 2    60   ~ 0
VCC
$Comp
L CONNECTOR P1
U 1 1 524A146C
P 9700 3750
F 0 "P1" H 10050 3850 70  0000 C CNN
F 1 "DEBUG" H 10050 3650 70  0000 C CNN
F 2 "" H 9700 3750 60  0001 C CNN
F 3 "" H 9700 3750 60  0001 C CNN
	1    9700 3750
	0    -1   -1   0   
$EndComp
Text Label 5300 4650 0    60   ~ 0
GND
Text Label 7550 4350 0    60   ~ 0
INT0
Text Label 7550 4450 0    60   ~ 0
INT1
Text Label 7550 4550 0    60   ~ 0
INT2
Text Label 9850 4550 2    60   ~ 0
SCL
Text Label 9850 4450 2    60   ~ 0
SDA
$Comp
L LED_M_8X8 D1
U 1 1 524B7872
P 1500 4200
F 0 "D1" H 1450 3800 60  0000 C CNN
F 1 "LED_M_8X8" H 1500 4200 60  0000 C CNN
F 2 "" H 1500 4200 60  0001 C CNN
F 3 "" H 1500 4200 60  0001 C CNN
	1    1500 4200
	1    0    0    -1  
$EndComp
Text Notes 1100 4400 0    60   ~ 0
YSM-1288CR3G2C
Entry Wire Line
	3100 3950 3200 4050
Entry Wire Line
	3100 4050 3200 4150
Entry Wire Line
	3100 4150 3200 4250
Entry Wire Line
	3100 4250 3200 4350
Entry Wire Line
	3100 4350 3200 4450
Entry Wire Line
	3100 4450 3200 4550
Entry Wire Line
	3100 4550 3200 4650
Entry Wire Line
	3100 4650 3200 4750
Entry Wire Line
	850  3050 950  3150
Entry Wire Line
	950  3050 1050 3150
Entry Wire Line
	1050 3050 1150 3150
Entry Wire Line
	1150 3050 1250 3150
Entry Wire Line
	1250 3050 1350 3150
Entry Wire Line
	1350 3050 1450 3150
Entry Wire Line
	1450 3050 1550 3150
Entry Wire Line
	1550 3050 1650 3150
Entry Wire Line
	1650 3050 1750 3150
Entry Wire Line
	1750 3050 1850 3150
Entry Wire Line
	1850 3050 1950 3150
Entry Wire Line
	1950 3050 2050 3150
Entry Wire Line
	2050 3050 2150 3150
Entry Wire Line
	2150 3050 2250 3150
Entry Wire Line
	2250 3050 2350 3150
Entry Wire Line
	1550 650  1650 750 
Entry Wire Line
	1650 650  1750 750 
Entry Wire Line
	1750 650  1850 750 
Entry Wire Line
	1850 650  1950 750 
Entry Wire Line
	1950 650  2050 750 
Entry Wire Line
	2050 650  2150 750 
Entry Wire Line
	2150 650  2250 750 
Entry Wire Line
	2250 650  2350 750 
Text Label 1650 900  1    60   ~ 0
C7
Text Label 1750 900  1    60   ~ 0
C6
Text Label 1850 900  1    60   ~ 0
C5
Text Label 1950 900  1    60   ~ 0
C4
Text Label 2050 900  1    60   ~ 0
C3
Text Label 2150 900  1    60   ~ 0
C2
Text Label 2250 900  1    60   ~ 0
C1
Text Label 2350 900  1    60   ~ 0
C0
Text Label 3100 4650 2    60   ~ 0
C7
Text Label 3100 4550 2    60   ~ 0
C6
Text Label 3100 4450 2    60   ~ 0
C5
Text Label 3100 4350 2    60   ~ 0
C4
Text Label 3100 4250 2    60   ~ 0
C3
Text Label 3100 4150 2    60   ~ 0
C2
Text Label 3100 4050 2    60   ~ 0
C1
Text Label 3100 3950 2    60   ~ 0
C0
Text Label 850  3300 1    60   ~ 0
R0
Text Label 950  3300 1    60   ~ 0
R1
Text Label 1050 3300 1    60   ~ 0
R2
Text Label 1150 3300 1    60   ~ 0
R3
Text Label 1250 3300 1    60   ~ 0
R4
Text Label 1350 3300 1    60   ~ 0
R5
Text Label 1450 3300 1    60   ~ 0
R6
Text Label 1550 3300 1    60   ~ 0
R7
Text Label 1650 3300 1    60   ~ 0
R8
Text Label 1750 3300 1    60   ~ 0
R9
Text Label 1850 3300 1    60   ~ 0
R10
Text Label 1950 3300 1    60   ~ 0
R11
Text Label 2050 3300 1    60   ~ 0
R12
Text Label 2150 3300 1    60   ~ 0
R13
Text Label 2250 3300 1    60   ~ 0
R14
Text Label 2350 3300 1    60   ~ 0
R15
Entry Wire Line
	700  2900 800  2800
Entry Wire Line
	700  2800 800  2700
Entry Wire Line
	700  2700 800  2600
Entry Wire Line
	700  2600 800  2500
Entry Wire Line
	700  2500 800  2400
Entry Wire Line
	700  2400 800  2300
Entry Wire Line
	700  2300 800  2200
Entry Wire Line
	700  2200 800  2100
Entry Wire Line
	700  2100 800  2000
Entry Wire Line
	700  2000 800  1900
Entry Wire Line
	700  1900 800  1800
Entry Wire Line
	700  1800 800  1700
Entry Wire Line
	700  1700 800  1600
Entry Wire Line
	700  1600 800  1500
Entry Wire Line
	700  1500 800  1400
Entry Wire Line
	700  1400 800  1300
Text Label 950  2800 2    60   ~ 0
R0
Text Label 950  2700 2    60   ~ 0
R1
Text Label 950  2600 2    60   ~ 0
R2
Text Label 950  2500 2    60   ~ 0
R3
Text Label 950  2400 2    60   ~ 0
R4
Text Label 950  2300 2    60   ~ 0
R5
Text Label 950  2200 2    60   ~ 0
R6
Text Label 950  2100 2    60   ~ 0
R7
Text Label 950  2000 2    60   ~ 0
R8
Text Label 950  1900 2    60   ~ 0
R9
Text Label 950  1800 2    60   ~ 0
R10
Text Label 950  1700 2    60   ~ 0
R11
Text Label 950  1600 2    60   ~ 0
R12
Text Label 950  1500 2    60   ~ 0
R13
Text Label 950  1400 2    60   ~ 0
R14
Text Label 950  1300 2    60   ~ 0
R15
$Comp
L R R5
U 1 1 52548F4F
P 9900 4950
F 0 "R5" V 9980 4950 50  0000 C CNN
F 1 "10k" V 9900 4950 50  0000 C CNN
F 2 "" H 9900 4950 60  0001 C CNN
F 3 "" H 9900 4950 60  0001 C CNN
	1    9900 4950
	-1   0    0    1   
$EndComp
$Comp
L R R6
U 1 1 52548F68
P 10100 4950
F 0 "R6" V 10180 4950 50  0000 C CNN
F 1 "10k" V 10100 4950 50  0000 C CNN
F 2 "" H 10100 4950 60  0001 C CNN
F 3 "" H 10100 4950 60  0001 C CNN
	1    10100 4950
	-1   0    0    1   
$EndComp
Text Label 10000 5500 1    60   ~ 0
VCC
Text Label 3150 1900 2    60   ~ 0
SDA
Text Label 3150 2000 2    60   ~ 0
SCL
$Comp
L C C4
U 1 1 525497FD
P 3550 2600
F 0 "C4" H 3600 2700 50  0000 L CNN
F 1 "1uF" H 3600 2500 50  0000 L CNN
F 2 "" H 3550 2600 60  0001 C CNN
F 3 "" H 3550 2600 60  0001 C CNN
	1    3550 2600
	0    -1   -1   0   
$EndComp
Text Label 4000 2700 2    60   ~ 0
GND
Text Label 3050 2400 3    60   ~ 0
VCC
Text Label 7400 4750 2    60   ~ 0
GND
Text Label 7400 4850 2    60   ~ 0
VCC
$Comp
L M4 J1
U 1 1 5254A123
P 9250 1750
F 0 "J1" H 9350 1700 60  0000 C CNN
F 1 "M4" H 9350 1800 60  0000 C CNN
F 2 "" H 9250 1750 60  0001 C CNN
F 3 "" H 9250 1750 60  0001 C CNN
	1    9250 1750
	1    0    0    -1  
$EndComp
Text Notes 6750 2800 0    60   ~ 0
ABM3B\nCTS 406
$Comp
L HT16K33 U1
U 1 1 5258B5FF
P 2000 1850
F 0 "U1" H 2000 1850 60  0000 C CNN
F 1 "HT16K33" H 2000 2800 60  0000 C CNN
F 2 "~" H 2000 1850 60  0000 C CNN
F 3 "~" H 2000 1850 60  0000 C CNN
	1    2000 1850
	-1   0    0    1   
$EndComp
Entry Wire Line
	750  3050 850  3150
Text Notes 8900 5950 0    60   ~ 0
PD7 = ~HWB\nPull low on reset to enter BSL\nImplement as jumper to GND
$Comp
L R R7
U 1 1 5259E5E5
P 9150 5250
F 0 "R7" V 9230 5250 50  0000 C CNN
F 1 "10k" V 9150 5250 50  0000 C CNN
F 2 "" H 9150 5250 60  0001 C CNN
F 3 "" H 9150 5250 60  0001 C CNN
	1    9150 5250
	0    1    1    0   
$EndComp
Text Label 9750 5250 2    60   ~ 0
VCC
$Comp
L SW_PUSH JP1
U 1 1 5259E702
P 9200 5500
F 0 "JP1" H 9350 5610 50  0000 C CNN
F 1 "SW_PUSH" H 9200 5420 50  0000 C CNN
F 2 "" H 9200 5500 60  0001 C CNN
F 3 "" H 9200 5500 60  0001 C CNN
	1    9200 5500
	1    0    0    -1  
$EndComp
Text Label 9750 5500 2    60   ~ 0
GND
$Comp
L SW_PUSH B1
U 1 1 5259EF7D
P 7950 5200
F 0 "B1" H 8100 5310 50  0000 C CNN
F 1 "SW_PUSH" H 7950 5120 50  0000 C CNN
F 2 "" H 7950 5200 60  0001 C CNN
F 3 "" H 7950 5200 60  0001 C CNN
	1    7950 5200
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH B2
U 1 1 5259EF83
P 7950 5500
F 0 "B2" H 8100 5610 50  0000 C CNN
F 1 "SW_PUSH" H 7950 5420 50  0000 C CNN
F 2 "" H 7950 5500 60  0001 C CNN
F 3 "" H 7950 5500 60  0001 C CNN
	1    7950 5500
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH B3
U 1 1 5259EF89
P 7950 5800
F 0 "B3" H 8100 5910 50  0000 C CNN
F 1 "SW_PUSH" H 7950 5720 50  0000 C CNN
F 2 "" H 7950 5800 60  0001 C CNN
F 3 "" H 7950 5800 60  0001 C CNN
	1    7950 5800
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH B4
U 1 1 5259EF92
P 7950 6100
F 0 "B4" H 8100 6210 50  0000 C CNN
F 1 "SW_PUSH" H 7950 6020 50  0000 C CNN
F 2 "" H 7950 6100 60  0001 C CNN
F 3 "" H 7950 6100 60  0001 C CNN
	1    7950 6100
	1    0    0    -1  
$EndComp
Text Label 7550 6400 1    60   ~ 0
GND
Text Notes 7650 6400 0    60   ~ 0
Pushbuttons use U2's\ninternal pull-ups.
$Comp
L R R8
U 1 1 525F480A
P 6200 6650
F 0 "R8" V 6300 6650 50  0000 C CNN
F 1 "10k" V 6200 6650 50  0000 C CNN
F 2 "" H 6200 6650 60  0001 C CNN
F 3 "" H 6200 6650 60  0001 C CNN
	1    6200 6650
	0    -1   -1   0   
$EndComp
$Comp
L R R9
U 1 1 525F4810
P 6200 6850
F 0 "R9" V 6100 6850 50  0000 C CNN
F 1 "10k" V 6200 6850 50  0000 C CNN
F 2 "" H 6200 6850 60  0001 C CNN
F 3 "" H 6200 6850 60  0001 C CNN
	1    6200 6850
	0    -1   -1   0   
$EndComp
Text Label 5850 6750 0    60   ~ 0
GND
$Comp
L C C7
U 1 1 525F4A44
P 6550 7150
F 0 "C7" H 6600 7250 50  0000 L CNN
F 1 "10nF" H 6600 7050 50  0000 L CNN
F 2 "" H 6550 7150 60  0001 C CNN
F 3 "" H 6550 7150 60  0001 C CNN
	1    6550 7150
	-1   0    0    1   
$EndComp
$Comp
L C C6
U 1 1 525F4A4A
P 6550 6350
F 0 "C6" H 6600 6450 50  0000 L CNN
F 1 "10nF" H 6600 6250 50  0000 L CNN
F 2 "" H 6550 6350 60  0001 C CNN
F 3 "" H 6550 6350 60  0001 C CNN
	1    6550 6350
	-1   0    0    1   
$EndComp
Text Label 6550 7350 3    60   ~ 0
GND
Text Label 6550 6150 1    60   ~ 0
GND
Text Label 4700 6800 2    60   ~ 0
GND
Wire Wire Line
	7650 3850 7750 3850
Wire Wire Line
	6950 3250 7650 3250
Wire Wire Line
	7650 3250 7650 3850
Wire Wire Line
	6550 3250 6500 3250
Wire Wire Line
	6500 3250 6500 4100
Wire Wire Line
	6500 3950 6550 3950
Wire Wire Line
	6500 3600 6300 3600
Connection ~ 6500 3600
Wire Wire Line
	7750 4050 6900 4050
Wire Wire Line
	6900 4050 6900 4100
Wire Wire Line
	6900 4100 6500 4100
Connection ~ 6500 3950
Wire Wire Line
	8650 3300 8650 2400
Wire Wire Line
	8550 3300 8550 2400
Wire Wire Line
	8650 2400 8700 2400
Wire Wire Line
	8550 2400 8500 2400
Wire Wire Line
	8850 3300 8850 3000
Wire Wire Line
	8850 3000 8950 3000
Wire Wire Line
	8750 3300 8750 2500
Wire Wire Line
	8750 2500 9350 2500
Wire Wire Line
	8850 2500 8850 1900
Wire Wire Line
	8450 2500 8450 3300
Wire Wire Line
	8250 2500 8450 2500
Wire Wire Line
	8350 1600 8350 3300
Connection ~ 8350 2500
Wire Wire Line
	7850 2500 7650 2500
Wire Wire Line
	9350 3000 9550 3000
Wire Wire Line
	9350 2500 9350 3000
Connection ~ 8850 2500
Wire Wire Line
	8350 2150 8100 2150
Connection ~ 8350 2150
Wire Wire Line
	7550 4150 7750 4150
Wire Wire Line
	9600 3850 9800 3850
Wire Wire Line
	10300 3850 10550 3850
Wire Wire Line
	9700 3750 9700 3850
Connection ~ 9700 3850
Wire Wire Line
	7100 3300 7100 3250
Connection ~ 7100 3250
Wire Wire Line
	7100 3950 7100 3900
Connection ~ 7100 3950
Wire Wire Line
	6950 3950 7750 3950
Wire Wire Line
	7350 4550 7150 4550
Wire Wire Line
	7150 4650 7450 4650
Wire Wire Line
	7350 4550 7350 4350
Wire Wire Line
	7350 4350 7750 4350
Wire Wire Line
	7450 4650 7450 4450
Wire Wire Line
	7450 4450 7750 4450
Wire Wire Line
	4500 5050 7550 5050
Wire Wire Line
	7550 5050 7550 4550
Wire Wire Line
	7550 4550 7750 4550
Wire Wire Line
	9600 4550 10250 4550
Wire Wire Line
	9600 4450 10250 4450
Wire Wire Line
	950  3150 950  3300
Wire Wire Line
	1050 3150 1050 3300
Wire Wire Line
	1150 3150 1150 3300
Wire Wire Line
	1250 3150 1250 3300
Wire Wire Line
	1350 3150 1350 3300
Wire Wire Line
	1450 3150 1450 3300
Wire Wire Line
	1550 3150 1550 3300
Wire Wire Line
	1650 3150 1650 3300
Wire Wire Line
	1750 3150 1750 3300
Wire Wire Line
	1850 3150 1850 3300
Wire Wire Line
	1950 3150 1950 3300
Wire Wire Line
	2050 3150 2050 3300
Wire Wire Line
	2150 3150 2150 3300
Wire Wire Line
	2250 3150 2250 3300
Wire Wire Line
	2350 3150 2350 3300
Wire Bus Line
	700  3050 2250 3050
Wire Wire Line
	1650 900  1650 750 
Wire Wire Line
	1750 750  1750 900 
Wire Wire Line
	1850 750  1850 900 
Wire Wire Line
	1950 750  1950 900 
Wire Wire Line
	2050 750  2050 900 
Wire Wire Line
	2150 750  2150 900 
Wire Wire Line
	2250 750  2250 900 
Wire Wire Line
	2350 750  2350 900 
Wire Bus Line
	1550 650  3200 650 
Wire Wire Line
	3100 4650 2950 4650
Wire Wire Line
	2950 4550 3100 4550
Wire Wire Line
	2950 4450 3100 4450
Wire Wire Line
	2950 4350 3100 4350
Wire Wire Line
	2950 4250 3100 4250
Wire Wire Line
	2950 4150 3100 4150
Wire Wire Line
	2950 4050 3100 4050
Wire Wire Line
	2950 3950 3100 3950
Wire Wire Line
	800  2800 950  2800
Wire Wire Line
	800  2700 950  2700
Wire Wire Line
	800  2600 950  2600
Wire Wire Line
	800  2500 950  2500
Wire Wire Line
	800  2400 950  2400
Wire Wire Line
	800  2300 950  2300
Wire Wire Line
	800  2200 950  2200
Wire Wire Line
	800  2100 950  2100
Wire Wire Line
	800  2000 950  2000
Wire Wire Line
	800  1900 950  1900
Wire Wire Line
	800  1800 950  1800
Wire Wire Line
	800  1700 950  1700
Wire Wire Line
	800  1600 950  1600
Wire Wire Line
	800  1500 950  1500
Wire Wire Line
	800  1400 950  1400
Wire Wire Line
	800  1300 950  1300
Wire Bus Line
	700  1400 700  3050
Wire Bus Line
	3200 650  3200 4750
Wire Wire Line
	9900 4550 9900 4700
Wire Wire Line
	10100 4450 10100 4700
Connection ~ 9900 4550
Connection ~ 10100 4450
Wire Wire Line
	9900 5200 9900 5250
Wire Wire Line
	10100 5250 10100 5200
Wire Wire Line
	9900 5250 10100 5250
Wire Wire Line
	10000 5250 10000 5500
Connection ~ 10000 5250
Wire Wire Line
	2950 2000 3150 2000
Wire Wire Line
	2950 1900 3150 1900
Wire Wire Line
	2950 2600 3350 2600
Wire Wire Line
	2950 2800 3800 2800
Wire Wire Line
	3800 2800 3800 2600
Wire Wire Line
	3800 2600 3750 2600
Connection ~ 3800 2700
Wire Wire Line
	3800 2700 4000 2700
Wire Wire Line
	3050 2600 3050 2400
Connection ~ 3050 2600
Wire Wire Line
	7150 4750 7400 4750
Wire Wire Line
	7400 4850 7150 4850
Wire Wire Line
	8850 1600 8350 1600
Wire Wire Line
	8850 1700 8500 1700
Wire Wire Line
	8500 1700 8500 1900
Wire Wire Line
	8850 1800 8700 1800
Wire Wire Line
	8700 1800 8700 1900
Wire Wire Line
	850  3150 850  3300
Wire Wire Line
	7650 5200 7550 5200
Wire Wire Line
	7550 5200 7550 6400
Wire Wire Line
	7650 6100 7550 6100
Connection ~ 7550 6100
Wire Wire Line
	7650 5800 7550 5800
Connection ~ 7550 5800
Wire Wire Line
	7650 5500 7550 5500
Connection ~ 7550 5500
Wire Wire Line
	8250 5200 8350 5200
Wire Wire Line
	8350 5200 8350 5100
Wire Wire Line
	8250 5500 8450 5500
Wire Wire Line
	8450 5500 8450 5100
Wire Wire Line
	8250 5800 8550 5800
Wire Wire Line
	8550 5800 8550 5100
Wire Wire Line
	8250 6100 8650 6100
Wire Wire Line
	8650 6100 8650 5100
Wire Wire Line
	8750 5100 8750 5500
Wire Wire Line
	8750 5500 8900 5500
Wire Wire Line
	8900 5250 8750 5250
Connection ~ 8750 5250
Wire Wire Line
	9500 5500 9750 5500
Wire Wire Line
	9400 5250 9750 5250
Wire Wire Line
	5850 6650 5950 6650
Wire Wire Line
	5950 6850 5850 6850
Wire Wire Line
	5850 6750 6050 6750
Wire Wire Line
	6450 6850 6800 6850
Wire Wire Line
	6550 6850 6550 6950
Wire Wire Line
	6450 6650 6800 6650
Wire Wire Line
	6550 6650 6550 6550
Wire Wire Line
	6550 7350 6550 7550
Wire Wire Line
	6550 6150 6550 5950
Wire Wire Line
	6100 4650 5300 4650
Wire Wire Line
	6050 4750 6050 5050
Wire Wire Line
	4500 5050 4500 6700
Connection ~ 6050 5050
$Comp
L R R4
U 1 1 525F5A22
P 5900 6350
F 0 "R4" V 6000 6350 50  0000 C CNN
F 1 "10k" V 5900 6350 50  0000 C CNN
F 2 "" H 5900 6350 60  0001 C CNN
F 3 "" H 5900 6350 60  0001 C CNN
	1    5900 6350
	-1   0    0    1   
$EndComp
$Comp
L R R10
U 1 1 525F5A32
P 5900 7150
F 0 "R10" V 5800 7150 50  0000 C CNN
F 1 "10k" V 5900 7150 50  0000 C CNN
F 2 "" H 5900 7150 60  0001 C CNN
F 3 "" H 5900 7150 60  0001 C CNN
	1    5900 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 6600 5900 6650
Connection ~ 5900 6650
Wire Wire Line
	5900 6850 5900 6900
Connection ~ 5900 6850
Wire Wire Line
	5900 7400 5900 7550
Wire Wire Line
	5900 6100 5900 5950
Text Label 5900 5950 3    60   ~ 0
VCC
Text Label 5900 7550 1    60   ~ 0
VCC
Connection ~ 6550 6650
Connection ~ 6550 6850
Text Label 6800 6650 2    60   ~ 0
INT0
Text Label 6800 6850 2    60   ~ 0
INT1
Text Notes 4000 7500 0    60   ~ 0
Encoders (all Bourns):\nE1 EM14 series (C0D-E24-L064S)\nE2 EPS series
Text Notes 5850 5200 0    60   ~ 0
Internal Pull-up INT2
Wire Wire Line
	6050 4750 6100 4750
$Comp
L ENC_MECH_PB E2
U 1 1 52617F38
P 5250 6750
F 0 "E2" H 5200 7050 60  0000 C CNN
F 1 "ENC_MECH_PB" H 5200 6400 60  0000 C CNN
F 2 "~" H 4850 6650 60  0000 C CNN
F 3 "~" H 4850 6650 60  0000 C CNN
	1    5250 6750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 6700 4700 6700
Wire Wire Line
	4700 6800 4500 6800
$Comp
L CRYSTAL X1
U 1 1 526328E5
P 7100 3600
F 0 "X1" H 7100 3750 60  0000 C CNN
F 1 "8 MHz" H 7100 3450 60  0000 C CNN
F 2 "~" H 7100 3600 60  0000 C CNN
F 3 "~" H 7100 3600 60  0000 C CNN
	1    7100 3600
	0    -1   -1   0   
$EndComp
Text Notes 5250 5600 0    60   ~ 0
Load one of\n- E1\n- E2, C6, C7, R8-10, R4
Text Notes 1150 6150 0    60   ~ 0
Rev 1.1 TODO\nBSL entered only on /RST asserted with PD7 low\nTie PD7 permanently low and add switch to /RST
$EndSCHEMATC
