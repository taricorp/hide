#include <avr/pgmspace.h>
#include <stdint.h>
#include "usb_keyboard_debug.h"

#define debug_flush usb_debug_flush_output
#define debug_print(s) _debug_print(PSTR(s))
#define debug_println(s) _debug_println(PSTR(s))
void debug_printx(uint8_t x);
#define debug_putchar usb_debug_putchar

void _debug_print(const char *s);
void _debug_println(const char *s);
