#include <stdint.h>

#define DISP_BLINK_OFF 0
#define DISP_BLINK_2HZ 1
#define DISP_BLINK_1HZ 2
#define DISP_BLINK_2S 3

extern uint16_t screen[8];

void display_init();
void display_set_blink(uint8_t rate);

void copyScreen();
void clearScreen();

