#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <util/delay.h>

#include "debug.h"
#include "display.h"

void lpwait() {
    wdt_reset();
    _delay_ms(10);
    wdt_reset();
    _delay_ms(20);
    wdt_reset();
}

void reboot_loader() {
    // Factory loader reset vector is at 0x1800 on the ATMega16u2
#if defined __AVR_ATmega16U2__
    __asm__ volatile("jmp 0x1800");
#else
# error Unrecognized MCU ID
#endif
}

uint8_t popcnt(uint8_t x) {
    uint8_t y = 0;
    while (x) {
        // Clear bottom-most set bit
        x &= x - 1;
        y += 1;
    }
    return y;
}

void gameOfLife(volatile char *terminate) {
    uint8_t board[8] = {
        0, 0x72, 2, 0xE2, 0x41, 0x4E, 0x42, 2
    };

    // Masks for cells adjacent
    const uint8_t masks[2][8] = {
        // .. row above and below
        { 0x83, 0x07, 0x0E, 0x1C,
          0x38, 0x70, 0xE0, 0xC1
        },
        // Current row
        { 0x82, 0x05, 0x0A, 0x14,
          0x28, 0x50, 0xA0, 0x41
        },
    };

    while (!*terminate) {
        uint8_t next_board[8] = {0};

        clearScreen();
        // Render frame. Live cells in green, dead in red.
        for (uint8_t y = 0; y < 8; y++) {
            for (uint8_t x = 0; x < 8; x++) {
                if (board[y] & _BV(x))
                    screen[y] |= _BV(x * 2 + 1);
                else
                    screen[y] |= _BV(x * 2);
            }
        }
        copyScreen();

        // Compute next frame
        for (uint8_t x = 0; x < 8; x++) {
            for (uint8_t y = 0; y < 8; y++) {
                uint8_t alive = (board[y] & (1 << x)) != 0;
                uint8_t above = board[y == 0 ? 7 : y - 1] & masks[0][x];
                uint8_t below = board[y == 7 ? 0 : y + 1] & masks[0][x];
                uint8_t adjacent = board[y] & masks[1][x];

                uint8_t n_adj = popcnt(above) + popcnt(below) + popcnt(adjacent);
                if (alive) {
                    // Cell dies if over- or under-crowded
                    if (n_adj < 2 || n_adj > 3)
                        alive = 0;
                } else if (n_adj == 3) {
                    // New cell is born
                    alive = 1;
                }

                next_board[y] |= alive << x;
            }
        }

        

        if (memcmp(board, next_board, sizeof(board)) == 0)
            reboot_loader();
        memcpy(board, next_board, sizeof(board));

        // Stall for approx. a half second
        for (uint8_t i = 0; i < 16; i++) {
            lpwait();
        }
    }
}

typedef enum {
    ERR_USBINIT = 1,
    ERR_WDTRESET = 0x55,
    ERR_OK = 0xAA
} err_t;

void unexpected_error(err_t code) {
    if (usb_configured()) {
        debug_print("Unexpected error: ");
        debug_printx(code);
        debug_flush();
    }

    cli();
    display_set_blink(DISP_BLINK_2S);
    memset(screen, code, sizeof(screen));
    copyScreen();
    reboot_loader();
}

int main(void)
{
    // Watchdog reset indicates a bug. Start the bootloader.
    if (MCUSR & _BV(WDRF))
        unexpected_error(ERR_WDTRESET);

    // Set watchdog: 64 ms early-warning, 128 ms reset.
    wdt_enable(WDTO_60MS);
    sei();

    /// Initialize clocks. We boot with the CPU at 1 MHz from the internal 8
    /// MHz RC oscillator.
    // Clear the CPU prescaler
    clock_prescale_set(0);

    // Bring up external clock
    CLKSEL0 |= _BV(EXTE);
    while (0 == (CLKSTA & _BV(EXTON)));
    // Set system clock prescaler to /2
    // TODO I2C connection seems to get flaky above about 100 kHz
    //CLKPR = _BV(CLKPCE);
    //CLKPR = 0;
    // Switch CPU clock to external oscillator
    CLKSEL0 |= _BV(CLKS);

    display_init();
    wdt_reset();

    // Set up USB and wait for ready
    usb_init();
    while (!usb_configured())
        wdt_reset();

    // We're online.
    for (int j = 0; j < 5; j++) {
        int i = 0;
        while(i++ < 100) {
            _delay_ms(50);
            wdt_reset();
        }
        debug_println("Hello, world!");
    }

    unexpected_error(ERR_OK);
}
