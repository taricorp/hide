#include <assert.h>
#include <stdint.h>
#include <string.h>
#include "lib/I2C/i2cmaster.h"
#include "display.h"

// 1110 a2 a1 a0 r/w
#define LCDD_ADDR 0xE0

#define START() i2c_start(LCDD_ADDR | I2C_WRITE)
#define RSTART() i2c_rep_start(LCDD_ADDR | I2C_WRITE)

uint16_t screen[8];

void display_init() {
    i2c_init();

    // Oscillator on
    START();
    i2c_write(0x21);
    // Turn on display, no blinking
    RSTART();
    i2c_write(0x81);
    // Set dimming: full brightness
    RSTART();
    i2c_write(0xEF);
    i2c_stop();

    clearScreen();
    copyScreen();
}

void copyScreen() {
    uint8_t *scr = (uint8_t *)screen;

    START();
    i2c_write(0);
    for (unsigned char i = 0; i < 16; i++) {
        i2c_write(*scr++);
    }
    i2c_stop();
}

void clearScreen() {
    memset(screen, 0, sizeof(screen));
}

/// Set display blink rate.
///
/// One of:
///  * DISP_BLINK_OFF: no blinking
///  * DISP_BLINK_2HZ: 2 Hz
///  * DISP_BLINK_1HZ: 1 Hz
///  * DISP_BLINK_2S: 0.5 Hz
void display_set_blink(uint8_t rate) {
    assert(rate <= DISP_BLINK_2S);
    START();
    i2c_write(0x81 | (rate << 1));
    i2c_stop();
}

/// Set display brightness, from 0 (minimum) to 15 (maximum).
void display_set_brightness(uint8_t brightness) {
    assert(brightness < 16);
    START();
    i2c_write(0xE0 | brightness);
    i2c_stop();
}
