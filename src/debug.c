#include "debug.h"

void _debug_print(const char *s) {
    char c;

    while ((c = pgm_read_byte(s++)) != 0) {
        debug_putchar(c);
    }
}

void _debug_println(const char *s) {
    _debug_print(s);
    debug_putchar('\n');
}

void debug_printx(uint8_t x) {
    const char CHARS[] = "0123456789ABCDEF";

    usb_debug_putchar(CHARS[x >> 4]);
    usb_debug_putchar(CHARS[x & 0xF]);
}
